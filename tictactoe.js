class Board {
    constructor() {
        this.fields = [
            [null, null, null],
            [null, null, null],
            [null, null, null]
        ];
    }

    place(x, y, mark) {
        checkBound(x, 'X', 0, 2);
        checkBound(y, 'Y', 0, 2);
        if (!this.fields[x][y]) {
            this.fields[x][y] = mark;
        }
    }

    fieldEmpty(x, y) {
        return !this.fields[x][y] ? true : false;
    }

    boardFull() {
        for (let i = 0; i < 3; i++) {
            for (let j = 0; j < 3; j++) {
                if (this.fieldEmpty(i, j)) {
                    return false;
                }
            }
        }
        return true;
    }

    getAllWinningPossibilites() {
        let rows = this.fields;
        let columns = [];
        for (let i = 0; i < this.fields.length; i++) {
            columns.push([
                this.fields[0][i],
                this.fields[1][i],
                this.fields[2][i]
            ]);
        }
        return rows.concat(columns, [
            [this.fields[0][0], this.fields[1][1], this.fields[2][2]],
            [this.fields[0][2], this.fields[1][1], this.fields[2][0]]
        ]);
    }

    * getRows() {
        for (let row of this.fields) {
            yield {data: row, type: 'row'};
        }
    }

    * getColumns() {
        for (let i = 0; i < this.fields.length; i++) {
            yield {
                data: [
                    this.fields[0][i],
                    this.fields[1][i],
                    this.fields[2][i]
                ],
                type: 'column'
            };
        }
    }

    * getDiagonals() {

        yield {
            data: [
                this.fields[0][0],
                this.fields[1][1],
                this.fields[2][2]
            ],
            type: 'diagonal'
        };
        yield {
            data: [
                this.fields[0][2],
                this.fields[1][1],
                this.fields[2][0]
            ],
            type: 'diagonal'
        };

    }
	

    * getPossibilities() {
        yield* this.getRows();
        yield* this.getColumns();
        yield* this.getDiagonals();
    }

    [Symbol.iterator]() {
        return this.getPossibilities();
    }
}


class TicTacToe {
    constructor() {
        this.board = new Board();
        this.currentPlayer = 'X';
    }

    placeMark(x, y) {
        if (this.board.fieldEmpty(x, y)) {
            this.board.place(x, y, this.currentPlayer);
            this.changePlayer();
        }
        let draw = true;
        for (let possibility of this.board) {
            if ((possibility.data[0] !== null) && (possibility.data[0] == possibility.data[1]) && (possibility.data[1] == possibility.data[2])) {
                console.log(possibility.data[0] + ' has won the game');
                draw = false;
            }
        }
        if (this.board.boardFull()) {
            if (draw) {
                console.log('draw game');
            }
            console.log('game has ended');
        }
    }

    changePlayer() {
        this.currentPlayer = this.currentPlayer === 'X' ? 'O' : 'X';
    }
}


let game = new TicTacToe();

function checkBound(value, name, min, max) {
    if (typeof value !== 'number' || value < min || value > max) {
        throw new Error(`${name} must be positive number and not greater than ${max}`);
    }
}

console.log(typeof game);
console.log(game.board instanceof Array);
console.log(Array.isArray(game.board));

try {
    game.placeMark(1, 2);
    game.placeMark(0, 2);
    game.placeMark(0, 1);
    game.placeMark(1, 1);
    game.placeMark(2, 0);
    game.placeMark(2, 2);
} catch (e) {
    console.log('Error ', e.message);
}
finally {
    console.log('Finally');
    console.log(game.board);
}

for (let pos of game.board) {
    console.log('iterator', pos);
}